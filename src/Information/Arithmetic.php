<?php
namespace Charm\Units\Information;

class Arithmetic {

    const ADD = 'add';
    const SUB = 'sub';
    const MUL = 'mul';
    const DIV = 'div';
    const POW = 'pow';

}
