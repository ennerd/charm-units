<?php
declare(strict_types=1);

namespace Charm\Units\Information;

use function number_format, usort, pow;

class Units {

    /**
     * Unit prefixes and their definition
     */
    const PREFIXES = [

        // No prefix
        [ 'name' => '',         'symbol' => '',     'base' => 10,   'pow' => 0, ],

        // SI prefixes
        [ 'name' => 'yotta',    'symbol' => 'Y',    'base' => 10,   'pow' => 24, ],
        [ 'name' => 'zetta',    'symbol' => 'Z',    'base' => 10,   'pow' => 21, ],
        [ 'name' => 'exa',      'symbol' => 'E',    'base' => 10,   'pow' => 18, ],
        [ 'name' => 'peta',     'symbol' => 'P',    'base' => 10,   'pow' => 15, ],
        [ 'name' => 'tera',     'symbol' => 'T',    'base' => 10,   'pow' => 12, ],
        [ 'name' => 'giga',     'symbol' => 'G',    'base' => 10,   'pow' => 9, ],
        [ 'name' => 'mega',     'symbol' => 'M',    'base' => 10,   'pow' => 6, ],
        [ 'name' => 'kilo',     'symbol' => 'k',    'base' => 10,   'pow' => 3, ],
        [ 'name' => 'hecto',    'symbol' => 'h',    'base' => 10,   'pow' => 2, ],
        [ 'name' => 'deca',     'symbol' => 'da',   'base' => 10,   'pow' => 1, ],
        [ 'name' => 'deci',     'symbol' => 'd',    'base' => 10,   'pow' => -1, ],
        [ 'name' => 'centi',    'symbol' => 'c',    'base' => 10,   'pow' => -2, ],
        [ 'name' => 'milli',    'symbol' => 'm',    'base' => 10,   'pow' => -3, ],
        [ 'name' => 'micro',    'symbol' => 'μ',    'base' => 10,   'pow' => -6, ],
        [ 'name' => 'nano',     'symbol' => 'n',    'base' => 10,   'pow' => -9, ],
        [ 'name' => 'pico',     'symbol' => 'p',    'base' => 10,   'pow' => -12, ],
        [ 'name' => 'femto',    'symbol' => 'f',    'base' => 10,   'pow' => -15, ],
        [ 'name' => 'atto',     'symbol' => 'a',    'base' => 10,   'pow' => -18, ],
        [ 'name' => 'zepto',    'symbol' => 'z',    'base' => 10,   'pow' => -21, ],
        [ 'name' => 'yocto',    'symbol' => 'y',    'base' => 10,   'pow' => -24, ],

        // Binary prefixes
        [ 'name' => 'yobi',     'symbol' => 'Yi',   'base' => 2, 'pow' => 80, ],
        [ 'name' => 'zebi',     'symbol' => 'Zi',   'base' => 2, 'pow' => 70, ],
        [ 'name' => 'exbi',     'symbol' => 'Ei',   'base' => 2, 'pow' => 60, ],
        [ 'name' => 'pebi',     'symbol' => 'Pi',   'base' => 2, 'pow' => 50, ],
        [ 'name' => 'tebi',     'symbol' => 'Ti',   'base' => 2, 'pow' => 40, ],
        [ 'name' => 'gibi',     'symbol' => 'Gi',   'base' => 2, 'pow' => 30, ],
        [ 'name' => 'mebi',     'symbol' => 'Mi',   'base' => 2, 'pow' => 20, ],
        [ 'name' => 'kibi',     'symbol' => 'Ki',   'base' => 2, 'pow' => 10, ],
        ];

    const DECIMAL_PREFIXES = ['Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k', 'h', 'da', 'd', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', '', ];
    const LOW_DECIMAL_PREFIXES = ['d', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', ];
    const HIGH_DECIMAL_PREFIXES = ['da', 'h', 'k', 'M', 'G', 'T', 'P', 'Z', 'Y', ];
    const BINARY_PREFIXES = ['Yi', 'Zi', 'Ei', 'Pi', 'Ti', 'Gi', 'Mi', 'Ki', '', ];
    const ALL_PREFIXES = ['Y', 'Z', 'E', 'P', 'T', 'G', 'M', 'k', 'h', 'da', 'd', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', 'Yi', 'Zi', 'Ei', 'Pi', 'Ti', 'Gi', 'Mi', 'Ki', '', ];

    // SI units
    const SECOND = 's';
    const METER = 'm'; // American-English spelling
    const METRE = 'm'; // English spelling
    const GRAM = 'g';
    const AMPERE = 'A';
    const KELVIN = 'K';
    const MOLE = 'mol';
    const CANDELA = 'cd';

    // SI derived units
    const BECQUEREL = 'Bq';
    const COULOMB = 'C';
    const CELSIUS = '°C';
    const FARAD = 'F';
    const GRAY = 'Gy';
    const HENRY = 'H';
    const HERTZ = 'Hz';
    const JOULE = 'J';
    const KATAL = 'kat';
    const LUMEN = 'lm';
    const LUX = 'lx';
    const NEWTON = 'N';
    const OHM = 'Ω';
    const PASCAL = 'Pa';
    const RADIAN = 'rad';
    const SIEMENS = 'S';
    const SIEVERT = 'Sv';
    const STERADIAN = 'sr';
    const TESLA = 'T';
    const VOLT = 'V';
    const WATT = 'W';
    const WEBER ='Wb';

    // Other units
    const FAHRENHEIT = '°F';
    const CURIE = 'Ci';
    const RUTHERFORD = 'Rd';
    const MINUTE = 'min';
    const HOUR = 'h';
    const DAY = 'd';
    const LIGHTYEAR = 'ly';

    // Binary units
    const BYTE = 'B';
    const BIT = 'b';

    /**
     * Information about various units
     */
    const UNITS = [

        self::AMPERE => [
            'name' => 'ampere',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['A'],
            ],

        self::BECQUEREL => [
            'name' => 'becquerel',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'baseUnit' => [ 1, self::SECOND, Arithmetic::DIV ], // 1/s
            'symbols' => ['Bq'],
            'baseUnit' => [
                self::SECOND, -1, Arithmetic::POW,
                ],
            ],

        self::BIT => [
            'name' => 'bit',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::ALL_PREFIXES,
            'symbols' => ['b'],
            ],

        self::BYTE => [
            'name' => 'byte',
            'commonPrefixes' => self::BINARY_PREFIXES,
            'legalPrefixes' => self::BINARY_PREFIXES,
            'symbols' => ['B'],
            ],

        self::CANDELA => [
            'name' => 'candela',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['cd'],
            ],

        self::CELSIUS => [
            'name' => 'Celsius',
            'commonPrefixes' => [ 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', ],
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['°C', 'C'],
            'convert' => [
                'to' => [
                    // 10 °C = 283.15 K - 273.15
                    self::KELVIN => [ 273.15, Arithmetic::SUB ],
                    ],
                'from' => [
                    self::KELVIN => [ 273.15, Arithmetic::ADD ],
                    ],
                ],
            ],

        self::COULOMB => [
            'name' => 'coulomb',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['C'],
            // SI derived
            'baseUnit' => [
                self::AMPERE,
                self::SECOND,
                Arithmetic::MUL,
                ],
            ],

        self::CURIE => [
            'name' => 'curie',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['Ci'],
            'convert' => [
                'from' => [
                    self::BECQUEREL => [ 3.7, 10, 10, Arithmetic::POW, Arithmetic::MUL, ],
                    ],
                'to' => [
                    self::BECQUEREL => [ 3.7, 10, 10, Arithmetic::POW, Arithmetic::DIV, ],
                    ],
                ],
            ],

        self::DAY => [
            'name' => 'day',
            'commonPrefixes' => [],
            'legalPrefixes' => [],
            'symbols' => ['d'],
            ],

        self::FARAD => [
            'name' => 'farad',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['F'],
            'baseUnit' => [
                self::AMPERE, 2, Arithmetic::POW, 
                self::GRAM, -1, Arithmetic::POW,
                self::METER, -2, Arithmetic::POW,
                self::SECOND, 4, Arithmetic::POW,
                self::MUL,
                self::MUL,
                self::MUL,
                ],
            ],
        self::FAHRENHEIT => [
            'name' => 'Fahrenheit',
            'commonPrefixes' => [ 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', ],
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['°F', 'F'],
            'convert' => [
                'from' => [
                    self::KELVIN => [ 9, Arithmetic::MUL, 5, Arithmetic::DIV, 459.67, Arithmetic::SUB ],
                    ],
                ],
                'to' => [
                    self::KELVIN => [ 459.67, Arithmetic::ADD, 5, Arithmetic::MUL, 9, Arithmetic::DIV ],
                    ],
                ],
            ],

        self::GRAM => [
            // the base unit is kg, but this should automatically be handled
            'name' => 'gram',
            'commonPrefixes' => [ 'k', 'm', 'µ', 'n', 'p', 'G', 'T', 'P', 'E', 'Z', 'Y', '', ],
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['g'],
            ],

        self::GRAY => [
            'name' => 'gray',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['Gy'],
            ],

        self::HENRY => [
            'name' => 'henry',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['H'],
            ],

        self::HERTZ => [
            'name' => 'hertz',
            'commonPrefixes' => self::HIGH_DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['Hz'],
            'siDerived' => [ self::SECOND, -1, Arithmetic::POW, ],
            'convert' => [
                'from' => [
                    self::SECOND => [ 1, Arithmetic::DIV ],
                    ],
                'to' => [
                    self::SECOND => [ 1, Arithmetic::DIV ],
                    ],
                ],
            ],

        self::HOUR => [
            'name' => 'hour',
            'commonPrefixes' => [],
            'legalPrefixes' => [],
            'symbols' => ['h', 'hr'],
            'convert' => [
                'to' => [
                    self::SECOND => [ 3600, Arithmetic::MUL ],
                    ],
                'from' => [
                    self::SECOND => [ 3600, Arithmetic::DIV ],
                    ],
            ],

        self::JOULE => [],

        self::KATAL => [],

        self::KELVIN => [
            'name' => 'kelvin',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['K'],
            ],

        self::LIGHTYEAR => [
            'name' => 'light-year',
            'commonPrefixes' => self::BIG_DECIMAL_PREFIXES,
            'legalPrexes' => self::DECIMAL_PREFIXES,
            'symbols' => ['ly'],
            'convert' => [
                'to' => [
                    self::METER => [ 9.4607, 10, 15, Arithmetic::POW, Arithmetic::MUL ],
                    ],
                'from' => [
                    self::METER => [ 9.4607, 10, 15, Arithmetic::POW, Arithmetic::DIV ],
                    ],
                ],
            ],

        self::LUMEN => [
            'name' => 'lumen',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['lm']
            ],

        self::LUX => [
            'name' => 'lux',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['lx'],
            ],

        self::METER => [
            'name' => 'meter',
            'commonPrefixes' => ['Y', 'Z', 'E', 'P', 'T', 'G', 'k', '', 'd', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y'],
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['m'],
            ],

        self::MINUTE => [
            'name' => 'minute',
            'commonPrefixes' => [''],
            'legalPrefixes' => [''],
            'symbols' => ['min'],
            ]

        self::MOLE => [
            'name' => 'mole',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['mol'],
            ],

        self::NEWTON => [
            'name' => 'newton',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::LEGAL_PREFIXES,
            'symbols' => ['N'],
            ],

        self::OHM => [
            'name' => 'ohm',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::LEGAL_PREFIXES,
            'symbols' => ['Ω'],
            ],

        self::PASCAL => [
            'name' => 'pascal',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::LEGAL_PREFIXES,
            'symbols' => ['Pa'],
            ],

        self::RADIAN => [
            'name' => 'radian',
            'commonPrefixes' => self::HIGH_DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['rad'],
            ],

        self::RUTHERFORD => [
            'name' => 'rutherford',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['Rd'], 
            'convert' => [
                'from' => [
                    self::BECQUEREL => [ 1000000, Arithmetic::DIV ],
                    ],
                'to' => [
                    self::BECQUEREL => [ 1000000, Arithmetic::MUL ],
                    ],
                ],
            ],

        self::SECOND => [
            'name' => 'second',
            'commonPrefixes' => [ 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', '', ],
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['s'],
            ],

        self::SIEMENS => [
            'name' => 'siemens',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['S'],
            ],

        self::SIEVERT => [
            'name' => 'sievert',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['Sv'],
            ],

        self::TESLA => [
            'name' => 'tesla',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => ['T'],
            ],

        self::VOLT => [
            'name' => 'volt',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => 'V',
            ],

        self::WATT => [
            'name' => 'watt',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => 'W',
            ],

        self::WEBER => [
            'name' => 'weber',
            'commonPrefixes' => self::DECIMAL_PREFIXES,
            'legalPrefixes' => self::DECIMAL_PREFIXES,
            'symbols' => 'Wb',
            ],
        ];

    const SECOND_PREFIXES = [ 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', ];
    const METER_PREFIXES = [ 'Y', 'Z', 'E', 'P', 'T', 'G', 'k', '', 'd', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y'];
    const GRAM_PREFIXES = [ 'k', 'm', 'µ', 'n', 'p', 'G', 'T', 'P', 'E', 'Z', 'Y', '', ];
    const AMPERE_PREFIXES = [ '
    const BYTE_PREFIXES = [ 'Yi', 'Zi', 'Ei', 'Pi', 'Ti', 'Gi', 'Mi', 'Ki', '' ];
    const BIT_PREFIXES = [ 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y', '', ];

    /**
     * Format a number for display with thousand separator and the requested number of decimals
     */
    public function number(float $number, int $minDecimals=null, int $maxDecimals=null, string $decimalSeparator=null, string $thousandSeparator=null): string {
        $thousandSeparator = $thousandSeparator ?? ' ';
        $decimalSeparator = $decimalSeparator ?? '.';
        $minDecimals = $minDecimals !== null ? $minDecimals : 1;
        $maxDecimals = $maxDecimals !== null ? $maxDecimals : 3;
        $i = $this->integerPart($number, $thousandSeparator);
        $d = $this->decimalPart($number, $maxDecimals, $minDecimals);
        $decimalSeparator = $decimalSeparator !== null ? $decimalSeparator : $this->decimalSeparator;

        if ($d !== '') {
            return "$i$decimalSeparator$d";
        } else {
            return $i;
        }
    }

    /**
     * Returns the integer part of a number with a thousand separator
     */
    protected function integerPart(float $number, string $thousandSeparator): string {
        return explode("\0", number_format( $number, 1, "\0", $thousandSeparator ))[0];
    }

    /**
     * Returns the decimal portion of a number, with a minimum and maximum number of digits
     * after the decimal separator.
     */
    protected function decimalPart(float $number, int $minDecimals, int $maxDecimals): string {
        $result = null;
        $decimals = $maxDecimals;
        $result = number_format($number, $decimals, ".", '');
        while ($decimals >= $minDecimals && substr($result, -1)==='0') {
            $result = number_format($number, --$decimals, ".", '');
        }
        $parts = explode(".", $result);

        return $parts[1] ?? '';
    }

    /**
     * Format a number of bytes using MiB, KiB, GiB etc.
     */
    public function byte(int $bytes, int $maxLength=3): string {
        $result = static::selectNumberPrefix($bytes, 1, 3, static::BYTE_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'B';
    }

    /**
     * Format a number of bits using bit, kbit, Mbit, Gbit, Tbit, Pbit etc.
     */
    public function bit(int $bits, int $maxLength=3): string {
        $result = static::selectNumberPrefix($bits, 1, $maxLength, static::BIT_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'bit';
    }

    /**
     * Format a number of meters using km, m, cm, mm etc
     */
    public function meter(float $meters, int $maxLength=3): string {
        $result = static::selectNumberPrefix($meters, 1, 3, static::METER_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'m';
    }

    public function kg(float $kilograms, int $maxLength=3): string {
        $result = static::selectNumberPrefix($kilograms * 1000, 1, 3, static::GRAM_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'g';
    }

    public function second(float $seconds): string {
        die("TO DO");
    }

    /**
     * Returns a tuple with a rounded number and the correct prefix to use
     * for the given number
     */
    protected static function selectNumberPrefix(float $value, int $minLength = 1, int $maxLength = 3, array $legalPrefixes): array {

        $prefixSet = [];
        foreach (static::NUMBER_PREFIXES as $prefix) {
            if (in_array($prefix['symbol'], $legalPrefixes)) {
                $prefixSet[] = $prefix;
            }
        }

        usort( $prefixSet, function($a, $b) {
            $a = pow($a['base'], $a['pow']);
            $b = pow($b['base'], $b['pow']);
            if ($a === $b) {
                return 0;
            } elseif ($a < $b) {
                return -1;
            } else {
                return 1;
            }
        } );

        $precision = max(0, $maxLength - $minLength);
        $candidates = [];
        $backups = [];

        foreach ($prefixSet as $prefix) {
            $order = pow($prefix['base'], $prefix['pow']);
            $formatted = round($value / $order, $precision + 1);

            if (strpos( (string) $formatted, 'E' ) !== false) {
                // Skip because of exponent
                continue;
            }

            // Remove decimals while length is too high
            while ($precision > 0 && strlen((string) $formatted) > $maxLength) {
                $formatted = round($formatted, --$precision);
            }

            $error = abs(($formatted * $order) - $value);

            if ($error == $value) {
                continue;
            }

            $l = strlen((string) $formatted);
            if ($l < $minLength || $l > $maxLength) {
                $backups[] = [ $formatted, $error, $prefix ];
            } else {
                $candidates[] = [ $formatted, $error, $prefix ];
            }
        }

        if (sizeof($candidates) === 0) {
            $candidates = $backups;
        }

        usort($candidates, function($a, $b) {
            $aL = strlen((string) $a[0]);
            $bL = strlen((string) $b[0]);
            if ($aL === $bL) {
                return 0;
            } elseif ($aL < $bL) {
                return -1;
            } else {
                return 1;
            }
        });

        return current($candidates);
    }

}
