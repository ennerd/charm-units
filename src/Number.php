<?php
declare(strict_types=1);

namespace Charm\Units;

use function number_format, usort, pow;

class Number {
    protected $value;

    protected $decimalSeparator = ".";
    protected $thousandSeparator = " ";
    protected $minDecimals = 0;
    protected $maxDecimals = 3;


    public function __construct(float $number) {
        $this->value = $number;
    }

    public function __toString() {
        $i = $this->integerPart($this->value, $this->thousandSeparator);
        $d = $this->decimalPart($this->value, $this->maxDecimals, $this->minDecimals);

        if ($d !== '') {
            return $i.$this->decimalSeparator.$d;
        } else {
            return $i;
        }
    }

    /**
     * Returns the integer part of a number with a thousand separator
     */
    protected function integerPart(float $number): string {
        return explode("\0", number_format( $number, 1, "\0", $this->thousandSeparator ))[0];
    }

    /**
     * Returns the decimal portion of a number, with a minimum and maximum number of digits
     * after the decimal separator.
     */
    protected function decimalPart(): string {
        $result = null;
        $decimals = $this->maxDecimals;
        $result = number_format($this->value, $decimals, ".", '');
        while ($decimals >= $this->minDecimals && substr($result, -1)==='0') {
            $result = number_format($this->value, --$decimals, ".", '');
        }
        $parts = explode(".", $result);

        return $parts[1] ?? '';
    }
}
