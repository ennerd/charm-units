<?php
declare(strict_types=1);

use Brick\Math\BigInteger;

namespace Charm\Units;

class Byte extends BigInteger {

    public function __construct($value=0) {
        parent::__construct($value);
    }

    public function __toString() {
        return $this->format
    }

}
