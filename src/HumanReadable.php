<?php
declare(strict_types=1);

namespace Charm\Units;

use function number_format, usort, pow;

class HumanReadable {

    /**
     * SI-prefixes and symbols table
     */
    const NUMBER_PREFIXES = [

        // No prefix
        [ 'name' => '',         'symbol' => '',     'base' => 10,   'pow' => 0, ],

        // SI prefixes
        [ 'name' => 'yotta',    'symbol' => 'Y',    'base' => 10,   'pow' => 24, ],
        [ 'name' => 'zetta',    'symbol' => 'Z',    'base' => 10,   'pow' => 21, ],
        [ 'name' => 'exa',      'symbol' => 'E',    'base' => 10,   'pow' => 18, ],
        [ 'name' => 'peta',     'symbol' => 'P',    'base' => 10,   'pow' => 15, ],
        [ 'name' => 'tera',     'symbol' => 'T',    'base' => 10,   'pow' => 12, ],
        [ 'name' => 'giga',     'symbol' => 'G',    'base' => 10,   'pow' => 9, ],
        [ 'name' => 'mega',     'symbol' => 'M',    'base' => 10,   'pow' => 6, ],
        [ 'name' => 'kilo',     'symbol' => 'k',    'base' => 10,   'pow' => 3, ],
        [ 'name' => 'hecto',    'symbol' => 'h',    'base' => 10,   'pow' => 2, ],
        [ 'name' => 'deca',     'symbol' => 'da',   'base' => 10,   'pow' => 1, ],
        [ 'name' => 'deci',     'symbol' => 'd',    'base' => 10,   'pow' => -1, ],
        [ 'name' => 'centi',    'symbol' => 'c',    'base' => 10,   'pow' => -2, ],
        [ 'name' => 'milli',    'symbol' => 'm',    'base' => 10,   'pow' => -3, ],
        [ 'name' => 'micro',    'symbol' => 'μ',    'base' => 10,   'pow' => -6, ],
        [ 'name' => 'nano',     'symbol' => 'n',    'base' => 10,   'pow' => -9, ],
        [ 'name' => 'pico',     'symbol' => 'p',    'base' => 10,   'pow' => -12, ],
        [ 'name' => 'femto',    'symbol' => 'f',    'base' => 10,   'pow' => -15, ],
        [ 'name' => 'atto',     'symbol' => 'a',    'base' => 10,   'pow' => -18, ],
        [ 'name' => 'zepto',    'symbol' => 'z',    'base' => 10,   'pow' => -21, ],
        [ 'name' => 'yocto',    'symbol' => 'y',    'base' => 10,   'pow' => -24, ],

        // Binary prefixes
        [ 'name' => 'yobi',     'symbol' => 'Yi',   'base' => 2, 'pow' => 80, ],
        [ 'name' => 'zebi',     'symbol' => 'Zi',   'base' => 2, 'pow' => 70, ],
        [ 'name' => 'exbi',     'symbol' => 'Ei',   'base' => 2, 'pow' => 60, ],
        [ 'name' => 'pebi',     'symbol' => 'Pi',   'base' => 2, 'pow' => 50, ],
        [ 'name' => 'tebi',     'symbol' => 'Ti',   'base' => 2, 'pow' => 40, ],
        [ 'name' => 'gibi',     'symbol' => 'Gi',   'base' => 2, 'pow' => 30, ],
        [ 'name' => 'mebi',     'symbol' => 'Mi',   'base' => 2, 'pow' => 20, ],
        [ 'name' => 'kibi',     'symbol' => 'Ki',   'base' => 2, 'pow' => 10, ],
        ];

    /**
     * Symbols available for various units
     */
    const METER_PREFIXES = [ 'Y', 'Z', 'E', 'P', 'T', 'G', 'k', '', 'd', 'c', 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y'];
    const BYTE_PREFIXES = [ 'Yi', 'Zi', 'Ei', 'Pi', 'Ti', 'Gi', 'Mi', 'Ki', '' ];
    const BIT_PREFIXES = [ 'k', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y', '', ];
    const GRAM_PREFIXES = [ 'k', 'm', 'µ', 'n', 'p', 'G', 'T', 'P', 'E', 'Z', 'Y', '', ];
    const SECOND_PREFIXES = [ 'm', 'μ', 'n', 'p', 'f', 'a', 'z', 'y', ];

    /**
     * Format a number for display with thousand separator and the requested number of decimals
     */
    public function number(float $number, int $minDecimals=null, int $maxDecimals=null, string $decimalSeparator=null, string $thousandSeparator=null): string {
        $thousandSeparator = $thousandSeparator ?? ' ';
        $decimalSeparator = $decimalSeparator ?? '.';
        $minDecimals = $minDecimals !== null ? $minDecimals : 1;
        $maxDecimals = $maxDecimals !== null ? $maxDecimals : 3;
        $i = $this->integerPart($number, $thousandSeparator);
        $d = $this->decimalPart($number, $maxDecimals, $minDecimals);
        $decimalSeparator = $decimalSeparator !== null ? $decimalSeparator : $this->decimalSeparator;

        if ($d !== '') {
            return "$i$decimalSeparator$d";
        } else {
            return $i;
        }
    }

    /**
     * Returns the integer part of a number with a thousand separator
     */
    protected function integerPart(float $number, string $thousandSeparator): string {
        return explode("\0", number_format( $number, 1, "\0", $thousandSeparator ))[0];
    }

    /**
     * Returns the decimal portion of a number, with a minimum and maximum number of digits
     * after the decimal separator.
     */
    protected function decimalPart(float $number, int $minDecimals, int $maxDecimals): string {
        $result = null;
        $decimals = $maxDecimals;
        $result = number_format($number, $decimals, ".", '');
        while ($decimals >= $minDecimals && substr($result, -1)==='0') {
            $result = number_format($number, --$decimals, ".", '');
        }
        $parts = explode(".", $result);

        return $parts[1] ?? '';
    }

    /**
     * Format a number of bytes using MiB, KiB, GiB etc.
     */
    public function byte(int $bytes, int $maxLength=3): string {
        $result = static::selectNumberPrefix($bytes, 1, 3, static::BYTE_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'B';
    }

    /**
     * Format a number of bits using bit, kbit, Mbit, Gbit, Tbit, Pbit etc.
     */
    public function bit(int $bits, int $maxLength=3): string {
        $result = static::selectNumberPrefix($bits, 1, $maxLength, static::BIT_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'bit';
    }

    /**
     * Format a number of meters using km, m, cm, mm etc
     */
    public function meter(float $meters, int $maxLength=3): string {
        $result = static::selectNumberPrefix($meters, 1, 3, static::METER_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'m';
    }

    public function kg(float $kilograms, int $maxLength=3): string {
        $result = static::selectNumberPrefix($kilograms * 1000, 1, 3, static::GRAM_PREFIXES);
        return $this->number($result[0], 0, 3).' '.$result[2]['symbol'].'g';
    }

    public function second(float $seconds): string {
        die("TO DO");
    }

    /**
     * Returns a tuple with a rounded number and the correct prefix to use
     * for the given number
     */
    protected static function selectNumberPrefix(float $value, int $minLength = 1, int $maxLength = 3, array $legalPrefixes): array {

        $prefixSet = [];
        foreach (static::NUMBER_PREFIXES as $prefix) {
            if (in_array($prefix['symbol'], $legalPrefixes)) {
                $prefixSet[] = $prefix;
            }
        }

        usort( $prefixSet, function($a, $b) {
            $a = pow($a['base'], $a['pow']);
            $b = pow($b['base'], $b['pow']);
            if ($a === $b) {
                return 0;
            } elseif ($a < $b) {
                return -1;
            } else {
                return 1;
            }
        } );

        $precision = max(0, $maxLength - $minLength);
        $candidates = [];
        $backups = [];

        foreach ($prefixSet as $prefix) {
            $order = pow($prefix['base'], $prefix['pow']);
            $formatted = round($value / $order, $precision + 1);

            if (strpos( (string) $formatted, 'E' ) !== false) {
                // Skip because of exponent
                continue;
            }

            // Remove decimals while length is too high
            while ($precision > 0 && strlen((string) $formatted) > $maxLength) {
                $formatted = round($formatted, --$precision);
            }

            $error = abs(($formatted * $order) - $value);

            if ($error == $value) {
                continue;
            }

            $l = strlen((string) $formatted);
//echo "length=$l v=$formatted\n";
            if ($l < $minLength || $l > $maxLength) {
                $backups[] = [ $formatted, $error, $prefix ];
            } else {
                $candidates[] = [ $formatted, $error, $prefix ];
            }
        }

        if (sizeof($candidates) === 0) {
            $candidates = $backups;
        }

        usort($candidates, function($a, $b) {
            $aL = strlen((string) $a[0]);
            $bL = strlen((string) $b[0]);
            if ($aL === $bL) {
                return 0;
            } elseif ($aL < $bL) {
                return -1;
            } else {
                return 1;
            }
        });

        return current($candidates);
    }

}
